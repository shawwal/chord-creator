import Rebase from 're-base';
import firebase from 'firebase';

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID
  // apiKey: 'AIzaSyDV7hvDcyEJA3JhylZ2RJHD0x0eCd5UwNY',
  // authDomain: 'https://chord-creator-2fdce.firebaseio.com',
  // databaseURL: 'https://chord-creator-2fdce.firebaseio.com',
  // projectId: 'chord-creator-2fdce',
  // storageBucket: 'chord-creator-2fdce.appspot.com',
  // messagingSenderId: '499605982114'
};

const app = firebase.initializeApp(config)
const base = Rebase.createClass(app.database())
const facebookProvider = new firebase.auth.FacebookAuthProvider()

export { app, base, facebookProvider }